<?php
/**
 * @file
 * ethical_events.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function ethical_events_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'oe_event_footer';
  $mini->category = '';
  $mini->admin_title = 'Event footer';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '62103ad4-bf17-49ec-9879-899c92a537e8';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-19171afc-f9bf-4575-b648-ffc60cbbf922';
  $pane->panel = 'contentmain';
  $pane->type = 'block';
  $pane->subtype = 'disqus-disqus_comments';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '19171afc-f9bf-4575-b648-ffc60cbbf922';
  $display->content['new-19171afc-f9bf-4575-b648-ffc60cbbf922'] = $pane;
  $display->panels['contentmain'][0] = 'new-19171afc-f9bf-4575-b648-ffc60cbbf922';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-19171afc-f9bf-4575-b648-ffc60cbbf922';
  $mini->display = $display;
  $export['oe_event_footer'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'oe_event_main_content';
  $mini->category = '';
  $mini->admin_title = 'Event main content';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '56c253e1-7fe6-41e1-9e94-12ef886ed7c9';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-ac82879c-e735-4541-bb54-6ab08d767ca4';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => 'medium',
      'image_link' => '',
    ),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ac82879c-e735-4541-bb54-6ab08d767ca4';
  $display->content['new-ac82879c-e735-4541-bb54-6ab08d767ca4'] = $pane;
  $display->panels['contentmain'][0] = 'new-ac82879c-e735-4541-bb54-6ab08d767ca4';
  $pane = new stdClass();
  $pane->pid = 'new-6b16df49-f90d-41b7-bd5a-a37aafc8c18a';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '6b16df49-f90d-41b7-bd5a-a37aafc8c18a';
  $display->content['new-6b16df49-f90d-41b7-bd5a-a37aafc8c18a'] = $pane;
  $display->panels['contentmain'][1] = 'new-6b16df49-f90d-41b7-bd5a-a37aafc8c18a';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-ac82879c-e735-4541-bb54-6ab08d767ca4';
  $mini->display = $display;
  $export['oe_event_main_content'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'oe_event_meta_data';
  $mini->category = '';
  $mini->admin_title = 'Event meta data';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'ea7d9d87-fde0-4a32-bd6e-f3a5a320ce30';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f5918c08-beb3-4e2d-87ab-10d8fea678fc';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_field_start_end_date_tz';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'title',
    'formatter' => 'ethical_event_date',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f5918c08-beb3-4e2d-87ab-10d8fea678fc';
  $display->content['new-f5918c08-beb3-4e2d-87ab-10d8fea678fc'] = $pane;
  $display->panels['contentmain'][0] = 'new-f5918c08-beb3-4e2d-87ab-10d8fea678fc';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-f5918c08-beb3-4e2d-87ab-10d8fea678fc';
  $mini->display = $display;
  $export['oe_event_meta_data'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'oe_event_side_bar';
  $mini->category = '';
  $mini->admin_title = 'Event side bar';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '461ae923-5dfa-4648-9669-9bbf6ed1a760';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-bf0f3643-540d-428b-a6f8-dc38ee0f8dc7';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_map_address';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'simple_gmap',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'include_map' => 1,
      'include_static_map' => 0,
      'include_link' => 0,
      'include_text' => 0,
      'iframe_height' => '286',
      'iframe_width' => '600',
      'zoom_level' => '15',
      'information_bubble' => 0,
      'link_text' => 'View larger map',
      'map_type' => 'm',
      'langcode' => 'en',
    ),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'bare_bones',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'bf0f3643-540d-428b-a6f8-dc38ee0f8dc7';
  $display->content['new-bf0f3643-540d-428b-a6f8-dc38ee0f8dc7'] = $pane;
  $display->panels['contentmain'][0] = 'new-bf0f3643-540d-428b-a6f8-dc38ee0f8dc7';
  $pane = new stdClass();
  $pane->pid = 'new-f35b75ea-4287-491c-9f45-ca4a428707c2';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_map_address';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'title',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 1,
    'override_title_text' => 'Location',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'f35b75ea-4287-491c-9f45-ca4a428707c2';
  $display->content['new-f35b75ea-4287-491c-9f45-ca4a428707c2'] = $pane;
  $display->panels['contentmain'][1] = 'new-f35b75ea-4287-491c-9f45-ca4a428707c2';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['oe_event_side_bar'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'oe_event_tags';
  $mini->category = '';
  $mini->admin_title = 'Event tags';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'See more by';
  $display->uuid = 'd19e212d-8c45-4755-ae7f-9dafc24ba9ba';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-31818a14-27f9-4167-a06d-43b0a5931e01';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_project';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'inline',
    'formatter' => 'ethical_event_project',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'bare_bones',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '31818a14-27f9-4167-a06d-43b0a5931e01';
  $display->content['new-31818a14-27f9-4167-a06d-43b0a5931e01'] = $pane;
  $display->panels['contentmain'][0] = 'new-31818a14-27f9-4167-a06d-43b0a5931e01';
  $pane = new stdClass();
  $pane->pid = 'new-7300aff6-4553-4526-8a45-ac3e1193e272';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_categories';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'inline',
    'formatter' => 'ethical_event_category',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'bare_bones',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '7300aff6-4553-4526-8a45-ac3e1193e272';
  $display->content['new-7300aff6-4553-4526-8a45-ac3e1193e272'] = $pane;
  $display->panels['contentmain'][1] = 'new-7300aff6-4553-4526-8a45-ac3e1193e272';
  $pane = new stdClass();
  $pane->pid = 'new-958541f9-fcbf-4c61-a7ee-c38e8a8a1a18';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_country';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'inline',
    'formatter' => 'ethical_event_country',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'bare_bones',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '958541f9-fcbf-4c61-a7ee-c38e8a8a1a18';
  $display->content['new-958541f9-fcbf-4c61-a7ee-c38e8a8a1a18'] = $pane;
  $display->panels['contentmain'][2] = 'new-958541f9-fcbf-4c61-a7ee-c38e8a8a1a18';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-31818a14-27f9-4167-a06d-43b0a5931e01';
  $mini->display = $display;
  $export['oe_event_tags'] = $mini;

  return $export;
}
