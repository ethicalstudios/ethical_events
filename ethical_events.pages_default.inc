<?php
/**
 * @file
 * ethical_events.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function ethical_events_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'openethical_events';
  $page->task = 'page';
  $page->admin_title = 'Events';
  $page->admin_description = '';
  $page->path = 'events';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Events',
    'name' => 'menu-secondary-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_openethical_events_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'openethical_events';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'radix_burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'f090081a-2790-4c0d-9842-22143d10ef68';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-fcd35dcb-3f14-467c-997a-41b79271c74e';
  $pane->panel = 'contentmain';
  $pane->type = 'ethical_events_nav_pane';
  $pane->subtype = 'ethical_events_nav_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'bare_bones',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'fcd35dcb-3f14-467c-997a-41b79271c74e';
  $display->content['new-fcd35dcb-3f14-467c-997a-41b79271c74e'] = $pane;
  $display->panels['contentmain'][0] = 'new-fcd35dcb-3f14-467c-997a-41b79271c74e';
  $pane = new stdClass();
  $pane->pid = 'new-99c2452d-b945-47ac-8c44-b1b52f8dca62';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'openethical_events-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'teaser',
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
    'view_settings' => 'fields',
    'header_type' => 'none',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '99c2452d-b945-47ac-8c44-b1b52f8dca62';
  $display->content['new-99c2452d-b945-47ac-8c44-b1b52f8dca62'] = $pane;
  $display->panels['contentmain'][1] = 'new-99c2452d-b945-47ac-8c44-b1b52f8dca62';
  $pane = new stdClass();
  $pane->pid = 'new-fc36a3fe-27d7-4552-afd4-7718a1da6879';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-3XnGmVLQDpQvfN7i81rvToySjgwa3mBg';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'fc36a3fe-27d7-4552-afd4-7718a1da6879';
  $display->content['new-fc36a3fe-27d7-4552-afd4-7718a1da6879'] = $pane;
  $display->panels['sidebar'][0] = 'new-fc36a3fe-27d7-4552-afd4-7718a1da6879';
  $pane = new stdClass();
  $pane->pid = 'new-fa65eab1-c666-4c67-a058-e8bdb0ae7ddc';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-zkbqUu0G5joY0tzUhV1QA961GxuA4BC5';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Category',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'fa65eab1-c666-4c67-a058-e8bdb0ae7ddc';
  $display->content['new-fa65eab1-c666-4c67-a058-e8bdb0ae7ddc'] = $pane;
  $display->panels['sidebar'][1] = 'new-fa65eab1-c666-4c67-a058-e8bdb0ae7ddc';
  $pane = new stdClass();
  $pane->pid = 'new-4de4ff9b-638d-4691-b425-4475b7a8b692';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-fisBofOsgv5lQ9DTXIT2kopJWtDH7OU3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Country',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '4de4ff9b-638d-4691-b425-4475b7a8b692';
  $display->content['new-4de4ff9b-638d-4691-b425-4475b7a8b692'] = $pane;
  $display->panels['sidebar'][2] = 'new-4de4ff9b-638d-4691-b425-4475b7a8b692';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['openethical_events'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'openethical_events_archive';
  $page->task = 'page';
  $page->admin_title = 'Events archive';
  $page->admin_description = '';
  $page->path = 'events/archive';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'none',
    'title' => '',
    'name' => 'menu-secondary-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_openethical_events_archive_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'openethical_events_archive';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'radix_burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Events';
  $display->uuid = 'fd54ef44-4b01-4914-bc79-14132b68e4a2';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-342dfbd1-731c-47a5-b15a-6543a0cb098c';
  $pane->panel = 'contentmain';
  $pane->type = 'ethical_events_nav_pane';
  $pane->subtype = 'ethical_events_nav_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'bare_bones',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '342dfbd1-731c-47a5-b15a-6543a0cb098c';
  $display->content['new-342dfbd1-731c-47a5-b15a-6543a0cb098c'] = $pane;
  $display->panels['contentmain'][0] = 'new-342dfbd1-731c-47a5-b15a-6543a0cb098c';
  $pane = new stdClass();
  $pane->pid = 'new-faaeffb1-44d4-4f11-8829-0b37e731bcf1';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'openethical_events-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'teaser',
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
    'view_settings' => 'fields',
    'header_type' => 'none',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'faaeffb1-44d4-4f11-8829-0b37e731bcf1';
  $display->content['new-faaeffb1-44d4-4f11-8829-0b37e731bcf1'] = $pane;
  $display->panels['contentmain'][1] = 'new-faaeffb1-44d4-4f11-8829-0b37e731bcf1';
  $pane = new stdClass();
  $pane->pid = 'new-61492f0e-89e5-4ad4-b076-850b9437fa27';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-3XnGmVLQDpQvfN7i81rvToySjgwa3mBg';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '61492f0e-89e5-4ad4-b076-850b9437fa27';
  $display->content['new-61492f0e-89e5-4ad4-b076-850b9437fa27'] = $pane;
  $display->panels['sidebar'][0] = 'new-61492f0e-89e5-4ad4-b076-850b9437fa27';
  $pane = new stdClass();
  $pane->pid = 'new-82ba03d7-9c81-4855-bcbf-9e57e1df56ec';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-zkbqUu0G5joY0tzUhV1QA961GxuA4BC5';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Category',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '82ba03d7-9c81-4855-bcbf-9e57e1df56ec';
  $display->content['new-82ba03d7-9c81-4855-bcbf-9e57e1df56ec'] = $pane;
  $display->panels['sidebar'][1] = 'new-82ba03d7-9c81-4855-bcbf-9e57e1df56ec';
  $pane = new stdClass();
  $pane->pid = 'new-577609c4-b7c8-40f8-8e1b-8efbaec9cc96';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-fisBofOsgv5lQ9DTXIT2kopJWtDH7OU3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Country',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '577609c4-b7c8-40f8-8e1b-8efbaec9cc96';
  $display->content['new-577609c4-b7c8-40f8-8e1b-8efbaec9cc96'] = $pane;
  $display->panels['sidebar'][2] = 'new-577609c4-b7c8-40f8-8e1b-8efbaec9cc96';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['openethical_events_archive'] = $page;

  return $pages;

}
